/* ***** BEGIN LICENSE BLOCK *****
 *   Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is mongrel.
 *
 * The Initial Developer of the Original Code is
 * Menno Smits.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var mongrel = {

    LOG: function(msg) {
        var consoleService = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);
        consoleService.logStringMessage(msg);
    },

    onLoad: function() {
        // initialization code
        this.initialized = true;
        this.strings = document.getElementById("mongrel-strings");

        this.LOG("hi");
        this.disableNonMongrelKeys();
    },

    //XXX clean this up
    disableNonMongrelKeys: function() {
        // Ensure that any built-in key binding that conflicts with
        // one used by Mongrel gets disabled.
        var mongrel_keys = [];
        var other_keys = {};
        var key_nodes = document.getElementsByTagName("key");
        var key_node;
        var modifiers, mongrel_key, keystroke;
        var i, j, len, len2;

        j=0;
        for (i=0, len=key_nodes.length; i<len; i++) {
            key_node = key_nodes[i];
            if (key_node.getAttribute('mongrel') == '') {
                keystroke = key_node.getAttribute('key').toLowerCase();
                if (other_keys[keystroke] == undefined) other_keys[keystroke] = [];
                other_keys[keystroke].push(key_node);
                this.logKey(keystroke, key_node);
                j++;
            } else {
                mongrel_keys.push(key_node);
            }
        }
        this.LOG('mongrel keys: ' + mongrel_keys.length);
        this.LOG('non-mongrel keys: ' + j);

        for (i=0, len=mongrel_keys.length; i<len; i++) {
            mongrel_key = mongrel_keys[i];
            this.logKey('mongrel key: ', mongrel_key);
            keystroke = mongrel_key.getAttribute('key');
            modifiers = mongrel_key.getAttribute('modifiers');

            var matching_keys = other_keys[keystroke];
            if (matching_keys == undefined) continue;
            this.LOG('matched for: ' + keystroke);
            for (j=0, len2=matching_keys.length; j<len2; j++) {
                key_node = matching_keys[j];
                this.logKey('match', key_node);
                if (modifiers == key_node.getAttribute('modifiers').trim()) {
                    this.LOG('disabling: ' + key_node.id);
                    key_node.setAttribute('disabled', 'true');
                }
            }
        }
        this.LOG('disableNonMongrelKeys done');
    },

    logKey: function(msg, key) {
        keyPair = function(attr) {
            var value = key.getAttribute(attr);
            if (!value) value = 'none';
            return ' ' + attr + '=' + value;
        };
        this.LOG(msg + ' <key id='+key.id + keyPair('key') + keyPair('modifiers') + keyPair('keycode') + '>');
    },

    deleteThread: function() {
        this.LOG('deleteThread');
        goDoCommand('cmd_selectThread');
        goDoCommand('cmd_delete');
    },

    mailBodyScrollPageDown: function() {
        this.getContentWindow().scrollByPages(1);
    },

    mailBodyScrollPageUp: function() {
        this.getContentWindow().scrollByPages(-1);
    },

    scrollMessages: function(amount) {
        var curIdx = this.getCurrentMsgIdx();
        if (curIdx == null) return;
        var newIdx = Math.max(curIdx + amount, 0);
        newIdx = Math.min(newIdx, this.getLastMessageIdx());
        this.selectMessage(newIdx);
    },

    topMessage: function() {
        this.selectMessage(0);
    },

    bottomMessage: function() {
        var lastIndex = this.getLastMessageIdx();
        if (lastIndex == null) return;
        this.selectMessage(lastIndex);
    },

    getContentWindow: function() {
        var contentWindow = window.top._content;
        var rssiframe = contentWindow.document.getElementById('_mailrssiframe');
        // if we are displaying an RSS article, we really want to scroll the nested iframe
        if (rssiframe)
            contentWindow = rssiframe.contentWindow;
        return contentWindow;
    },

    selectMessage: function(index) {
        if (gDBView == null) return;
        var key = gDBView.getKeyAt(index);
        gDBView.selectMsgByKey(key);
    },

    getCurrentMsgIdx: function() {
        if (gDBView == null) return null;
        return gDBView.currentlyDisplayedMessage;
    },

    getLastMessageIdx: function() {
        if (gDBView == null) return null;
        return gDBView.rowCount - 1;
    },


    // The following 2 methods should be in their own extension
    selectMailViewByName: function(name) {
        this.LOG('selectMailViewByName');
        var idx = this.getMailViewIndex(name);
        this.LOG('idx: ' + idx);
        if (idx == null) return;
        ViewChange(idx);
    },

    getMailViewIndex: function(name) {
        var view;
        var list = Components.classes["@mozilla.org/messenger/mailviewlist;1"]
                             .getService(Components.interfaces.nsIMsgMailViewList);
        var len = list.mailViewCount;
        for (var i=0; i<len; i++) {
            view = list.getMailViewAt(i);
            this.LOG(view.mailViewName);
            if (view.mailViewName == name) return i + 9;
        }
        return null;
    }
};


window.addEventListener("load", function(e) { mongrel.onLoad(e); }, false);
