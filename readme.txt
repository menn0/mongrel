This is a fairly rough and simple extension to Thunderbird that
provides Mutt-like key bindings and functionality.

It is heavily used by the author every day but could do with some
cleaning up, documentation etc. If you're interested in using it
yourself, please email menno@freshfoo.com.
